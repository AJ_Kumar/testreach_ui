const loginCommands = {
    submitUser: function (user) {
      return this.waitForElementVisible('@emailField', 10000)
        .setValue('@emailField', user)
        .click('@submitEmailButton');
    },
    submitPassword: function (pass) {
      return this.waitForElementVisible('@passwordField', 5000)
        .setValue('@passwordField', pass)
        .click('@submitPasswordButton');
    },
    handleAdminPage: function (pass, isAdmin) {
      this.waitForElementVisible('@adminButton', 10000)
      const isAdminCandidatePage = this.api.elements('xpath', '@adminButton', function (result) {
        return result.status;
      });
      if (isAdminCandidatePage === -1) return;
      const navButton = isAdmin ? '@adminButton' : '@candidateButton';
      this.waitForElementVisible(navButton, 4000);
      this.click(navButton)
      .dismissPanel()
      .newAccountPage(pass);
      const navPage = isAdmin ? '@adminPage' : '@candidatePage';
      this.waitForElementVisible(navPage, 10000);
      //this.dismissPanel();
    },
    createNewPassword: function (pass) {
      this.waitForElementVisible('@newPasswordInput')
        .setValue('@newPasswordInput', pass)
        .setValue('@newPasswordConfirmInput', pass)
        .click('@newPasswordSubmitBtn');
    },
    login: function (user , pass , admin = true) {
      return this.submitUser(user)
        .submitPassword(pass)
        .waitForElementVisible('@confirmLogin', 10000);
        // .dismissPanel();
        this.api.pause(3000);
      // .handleAdminPage(admin);
    },
    dismissPanel:function(){
      this.api.pause(2000);
      const isDismissPage = this.api.elements('xpath', '@dismissPage', function (result) {
      return result.status;
      });
      if (isDismissPage === -1) return;
      return this.click('@dismissOkPopup')
    },
    newAccountPage:function(pass){
      this.api.pause(10000);
      const isNewAccountPage = this.api.elements('xpath', '@newAccountPage', function (result) {
        return result.status;
        });
      if (isNewAccountPage === -1) return;
      return this.waitForElementVisible('@checkBoxButton', 10000)
            .click('@checkBoxButton')
            .click('@nextButton')
            .waitForElementVisible('@oldPasswordField', 10000)
            .setValue('@oldPasswordField',pass)
            .setValue('@newPasswordField',pass)
            .setValue('@retypePasswordField',pass)
            .click('@submitResetPasswordButton')
            .waitForElementVisible('@successfulPopUp', 10000)
            .click('@successfulPopUp')
            //.waitForElementVisible('@timeZoneField', 10000)
            .pause(5000)
            //.click('@timeZoneField')
            //.waitForElementVisible('@timeZoneValue', 10000)
            .pause(5000)
            //.click('@timeZoneValue')
            .click('@submitTimeZoneButton')
            //.waitForElementVisible('@successfulPopUp', 10000)
            //.click('@successfulPopUp')
            //.waitForElementVisible('@finishButton', 10000)
            //.click('@finishButton')
            .pause(10000)
    },
  };
  
  module.exports = {
    url: function () {
      return this.api.launch_url;
    },
    commands: [loginCommands],
    elements: {
      emailField: '#username-field',
      passwordField: '#password-field',
      oldPasswordField: '#old_password',
      newPasswordField: '#new_password',
      retypePasswordField: '#re_password',
      submitResetPasswordButton: '#submit_password',
      submitEmailButton: '#next-btn',
      submitPasswordButton: '#login-btn',
      submitTimeZoneButton: '#submit_locales',
      successfulPopUp: 'button[class="btn btn-primary"]',
      finishButton: 'button[class="btn btn-success btn-next"]',
      timeZoneField: 'select[id="time_zone_name"]',
      timeZoneValue: 'option[value="Europe/Dublin"]',
      checkBoxButton: {
        selector: '//*[@class="lbl"]',
        locateStrategy: 'xpath'
      },
      nextButton: '#buttonNext',
      newPasswordInput: '#new-password',
      newPasswordConfirmInput: '#confirm-password',
      newPasswordSubmitBtn: 'button[class="width-35 btn btn-primary-log weight-management"]',
      confirmLogin: '#main-container',
      candidatePage: {
        selector: '//*[@id="candidate-exams"]',
        locateStrategy: 'xpath'
      },
      adminPage: {
        selector: '//*[@id="page-content"]',
        locateStrategy: 'xpath'
      },
      dismissPage: {
        selector: '//*[@id="loc-container"]',
        locateStrategy: 'xpath'
      },
      newAccountPage: {
        selector: '//*[@id="terms-conditions"]',
        locateStrategy: 'xpath'
      },
      dismissOkPopup: {
        selector: '//*[@id="ok-button"]',
        locateStrategy: 'xpath'
      },
      adminButton: {
        selector: '//span[text()="Admin Account"]',
        locateStrategy: 'xpath'
      },
      candidateButton: {
        selector: '//span[text()="Candidate Account"]',
        locateStrategy: 'xpath'
      },
    }
  };
  