const rootCommands = {
    dropdown:function (parentTag, childTag) {
      return this.waitForElementVisible(parentTag, 10000)
          .click(parentTag)
          .waitForElementVisible(childTag, 10000)
          .click(childTag);
    },
    navigateToOrganisation:function () {
      return this.waitForElementVisible('@addNewSiteAdmin', 10000)
      .dropdown('@setupDropDown', '@selectOrganisation');
    },
    navigateToSiteAdmin:function () {
      return this.dropdown('@setupDropDown', '@selectSiteAdmin');
    },
    addCompany:function (new_company) {
      return this.waitForElementVisible('@addNewCompany', 10000)
      .click('@addNewCompany')
      .waitForElementVisible('@nameField', 10000)
      .setValue('@nameField', new_company.name)
      .setValue('@emailField', new_company.email)
      .setValue('@addressField', new_company.address)
      .setValue('@loginKeyField', new_company.loginkey)
      //.setValue('@testTypeListField', 'Assessment')
      //.waitForElementVisible('@testTypeListValue', 10000)
      //.click('@testTypeListValue')
      //.api.pause(10000)
      .click('@saveButton')
      .waitForElementVisible('@successfulPopUp', 10000)
      .click('@successfulPopUp')
      
    },
    searchCompany:function (new_company) {
      return this.waitForElementVisible('@searchField', 10000)
      .setValue('@searchField', new_company.name)
      .getText('@getCompanyName', function(result) {
        this.assert.equal(result.value, new_company.name);
      })
      .getText('@getCompanyAddress', function(result) {
        this.assert.equal(result.value, new_company.address);
      })
    },
    deleteCompany:function () {
      return this.waitForElementVisible('@deleteOrganisation', 10000)
      .click('@deleteOrganisation')
      .waitForElementVisible('@deleteSuccessPopUp', 10000)
      .click('@deleteSuccessPopUp')
      .api.pause(10000);
    },
    addSiteAdmin:function () {
      return this.waitForElementVisible('@addNewSiteAdmin', 10000)
      .click('@addNewSiteAdmin')
      .waitForElementVisible('@deleteSuccessPopUp', 10000)
      .click('@deleteSuccessPopUp')
      .api.pause(10000);
    }
  };
  
  module.exports = {
    commands: [rootCommands],
    elements: {
      nameField: '#name',
      emailField: '#from_email',
      addressField: '#address_line1',
      loginKeyField: '#login_key',
      testTypeListField: '#s2id_autogen3',
      searchField: 'input[class="form-control input-sm"]',
      successfulPopUp: 'button[class="btn btn-primary"]',
      deleteSuccessPopUp: 'button[class="btn btn-sm btn-danger"]',
      deleteOrganisation: 'i[class="icon-trash delete-link"]',
      testTypeListValue: {
        selector: '//span[text()="Assessment"]',
        locateStrategy: 'xpath'
      },
      getCompanyName: {
        selector: '//*[@id="table_companies"]//td[1]',
        locateStrategy: 'xpath'
      },
      getCompanyAddress: {
        selector: '//*[@id="table_companies"]//td[2]',
        locateStrategy: 'xpath'
      },
      addNewSiteAdmin: '#add-new-user',
      addNewCompany: '#add-new-company',
      saveButton: '#save-button',
      setupDropDown: {
        selector: '//span[text()="Set-up"]',
        locateStrategy: 'xpath'
      },
      selectOrganisation: {
        selector: '//span[text()="Organizations"]',
        locateStrategy: 'xpath'
      },
      selectSiteAdmin: {
        selector: '//span[text()="Site Admins"]',
        locateStrategy: 'xpath'
      },
    }
  };
  