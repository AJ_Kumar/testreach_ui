const roleLists = ['ROLE_ADMIN_FACILITATOR', 'ROLE_ADMIN_MARKER', 'ROLE_ADMIN_MODERATOR', 
                  'ROLE_ADMIN_USER_FULL_RESULT', 'ROLE_ADMIN_USER_GUARDIAN', 'ROLE_ADMIN_USER_READ',
                  'ROLE_CANDIDATE_REVIEW', 'ROLE_CREATE_CANDIDATE_USER', 'ROLE_CANDIDATE', 
                  'ROLE_CREATE_ADMIN_USER', 'ROLE_INVIGILATOR', 'ROLE_CREATE_TEST_CENTRE', 
                  'ROLE_MANAGE_TESTS', 'ROLE_OBSERVATION_TESTS', 'ROLE_QUESTION_AUTHOR', 
                  'ROLE_TEST_AUTHOR'];
const siteAdminCommands = {
    dropdown:function (parentTag, childTag) {
      return this.waitForElementVisible(parentTag, 10000)
          .click(parentTag)
          .waitForElementVisible(childTag, 10000)
          .click(childTag);
    },
    navigateToSiteAdmin:function () {
        return this.waitForElementVisible('@addNewSiteAdmin', 10000)
        .dropdown('@setupDropDown', '@selectSiteAdmin');
    },
    selectAllRole:function () {
      return  roleLists.map( ( roleList ) => {
        return this.waitForElementVisible('@roleListField', 10000)
        .click('@roleListField')
        .setValue('@roleListField', roleList)
        .waitForElementVisible('@roleListValue', 10000)
        .click('@roleListValue')
      });
      
    },
    addSiteAdmin:function (new_admin) {
       this.waitForElementVisible('@addNewSiteAdmin', 10000)
      .click('@addNewSiteAdmin')
      .waitForElementVisible('@firstnameField', 10000)
      .setValue('@firstnameField', new_admin.firstname)
      .setValue('@lastnameField', new_admin.lastname)
      .waitForElementVisible('@organizationField', 10000)
      .click('@organizationField')
      .waitForElementVisible('@organizationListField', 10000)
      .setValue('@organizationListField', new_admin.organisation)
      .click('@organizationListValue')
      .waitForElementVisible('@emailField', 10000)
      .click('@emailField')
      .setValue('@emailField', new_admin.email)
      .waitForElementVisible('@passwordField', 10000)
      .click('@passwordField')
      .setValue('@passwordField', new_admin.password)
      .setValue('@testAdminsField', new_admin.maxtestadmin)
      .selectAllRole();
      return this.click('@saveAddSiteAdminButton');
    },
    deleteSiteAdmin:function (new_admin) {
      return this.waitForElementVisible('@deleteSiteAdmin', 10000)
      .click('@deleteSiteAdmin')
      .waitForElementVisible('@deleteSuccessPopUp', 10000)
      .click('@deleteSuccessPopUp')
    },
    searchSiteAdmin:function (new_admin) {
      return this.waitForElementVisible('@searchField', 10000)
      .setValue('@searchField', new_admin.email)
      .getText('@getOrganization', function(result) {
        this.assert.equal(result.value, new_admin.organisation);
      })
      .getText('@getUsername', function(result) {
        this.assert.equal(result.value, new_admin.email);
      })
      .getText('@getFirstname', function(result) {
        this.assert.equal(result.value, new_admin.firstname);
      })
      .getText('@getLastname', function(result) {
        this.assert.equal(result.value, new_admin.lastname);
      })
    }
  };
  
  module.exports = {
    commands: [siteAdminCommands],
    elements: {
      firstnameField: '#firstname',
      lastnameField: '#lastname',
      emailField:'input[id="username"]',/* {
        selector: '//*[@id="username"]',
        locateStrategy: 'xpath'
      },*/
      passwordField: {
        selector: '//*[@id="password"]',
        locateStrategy: 'xpath'
      },
      addressField: '#address_line1',
      loginKeyField: '#login_key',
      organizationField: 'span[class="select2-chosen"]',
      organizationListField: 'input[class="select2-input select2-focused"]',
      searchField: 'input[class="form-control input-sm"]',
      successfulPopUp: 'button[class="btn btn-primary"]',
      deleteSuccessPopUp: 'button[class="btn btn-sm btn-danger"]',
      deleteSiteAdmin: 'i[class="icon-trash delete-link"]',
      organizationListValue: 'span[class="select2-match"]',
      roleListField: '#s2id_autogen1',
      roleListValue: 'li[class="select2-results-dept-0 select2-result select2-result-selectable select2-highlighted"]',
      testAdminsField:'#max_test_admins',
      saveAddSiteAdminButton: '#addnewsiteadmin',
      addNewSiteAdmin: '#add-new-user',
      saveButton: '#save-button',
      setupDropDown: {
        selector: '//span[text()="Set-up"]',
        locateStrategy: 'xpath'
      },
      selectSiteAdmin: {
        selector: '//span[text()="Site Admins"]',
        locateStrategy: 'xpath'
      },
      getOrganization: {
        selector: '//*[@id="table_users"]//td[4]',
        locateStrategy: 'xpath'
      },
      getUsername: {
        selector: '//*[@id="table_users"]//td[3]',
        locateStrategy: 'xpath'
      },
      getFirstname: {
        selector: '//*[@id="table_users"]//td[1]',
        locateStrategy: 'xpath'
      },
      getLastname: {
        selector: '//*[@id="table_users"]//td[2]',
        locateStrategy: 'xpath'
      },
    }
  };
  