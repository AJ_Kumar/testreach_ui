const testrail = require('./custom_commands/testrail');
const before_execution = require('./custom_commands/before_execution');
const after_execution = require('./custom_commands/after_execution');

var test_execution_details;
module.exports = {
  
    before: async function(browser, done){
      await before_execution.test_run_automation();
      done();
    },
    after: function(browser, done){
      done();
    },
    // This will be run before each test suite is started
    beforeEach: function (browser, done) {
      browser.init();
      browser.maximizeWindow();
      //console.log(browser.currentTest);
      console.log("before each");
      // getting the session info
      //console.log(browser);
      browser.status(function (result) {
        //console.log(result);
        done();
      });
    },
  
    // This will be run after each test suite is finished
    afterEach: async function (browser, done) {
      await after_execution.update_automation_results(browser);
      done();
    }
  };
  