# TestReach_UI

##Pre-requisites

1. Install Java (JDK 8+): 

    https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html

    ```
    
    $ java --version
    ```

2. Install nodejs (8+) with nvm


    https://github.com/coreybutler/nvm-windows 

    https://nodejs.org/dist/v10.15.3/node-v10.15.3-x64.msi

3. Install Edge driver

    ```
    $ DISM.exe /Online /Add-Capability /CapabilityName:Microsoft.WebDriver~~~~0.0.1.0
    ```

4. Install npm package from Project directory

    ```
    $ npm install package.json
    ```

5. Test Rail API Document

    https://www.npmjs.com/package/testrail-api

    http://docs.gurock.com/testrail-api2/start

