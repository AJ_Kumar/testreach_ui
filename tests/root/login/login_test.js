const new_company = {
  name : 'automationOrg',
  email : 'automationOrg@testreach.com',
  address : 'Block 9-10 Nexus UCD, Belfield Office Park, Beech Hill Road, Dublin 4, D04 V2N9',
  loginkey : 'ao'
}
const new_admin = {
  firstname : 'automationOrg',
  lastname : 'admin',
  email : 'automationorg@testreach.com',
  password:'tr123',
  maxtestadmin : '1000',
  organisation : 'automationOrg'
}

module.exports = {
    'Login test': (browser) => {
      const page = browser.page.login.login_page();
      page.navigate()
        .login(browser.globals.root_login_id, browser.globals.root_password);
        //browser.pause(2000);
        //browser.end();
    },
    'Add Organisation test': (browser) => {
      const page = browser.page.root.organisation_page();
      page.navigateToOrganisation()
      .addCompany(new_company)
      .searchCompany(new_company);
        //browser.pause(2000);
        //browser.end();
    },
    'Delete Organisation test': (browser) => {
      const page = browser.page.root.organisation_page();
      //page.navigateToOrganisation()
      page.searchCompany(new_company)
      .deleteCompany()
        //browser.pause(2000);
        //browser.end();
    },
    'Add Site Admin': (browser) => {
      const page = browser.page.root.site_admin_page();
      page.navigateToSiteAdmin()
      .addSiteAdmin(new_admin)
      .searchSiteAdmin(new_admin);
        //browser.pause(2000);
        //browser.end();
    },
    'Delete Site Admin': (browser) => {
      const page = browser.page.root.site_admin_page();
      //page.navigateToSiteAdmin()
      page.searchSiteAdmin(new_admin)
      .deleteSiteAdmin(new_admin);
        //browser.pause(2000);
        //browser.end();
    },
  };