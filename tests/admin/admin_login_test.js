module.exports = {
  '@tags': ['login'],
  '[4468, C4473] Admin Login test': (browser) => {
    const page = browser.page.login.login_page();
    page.navigate()
      .login(browser.globals.admin_login_id, browser.globals.admin_password);
      //browser.pause(2000);
      //browser.end();
  },
'[C5056] Admin Account test': (browser) => {
    const page = browser.page.login.login_page();
    const isAdmin = true;
    page.handleAdminPage(browser.globals.admin_password, isAdmin);
      //browser.pause(2000);
      //browser.end();
  }
}