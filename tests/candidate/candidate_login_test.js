module.exports = {
  '[C4472]Candidate Login test': (browser) => {
    const page = browser.page.login_page();
    page.navigate()
      .login(browser.globals.candidate_login_id, browser.globals.candidate_password);
      browser.pause(2000);
      browser.end();
  }
  }