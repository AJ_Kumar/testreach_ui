const testrail = require('./testrail.js');

const project_name = testrail.project_name;
const test_plan_name = testrail.test_plan_name;
const test_case_automated_type = testrail.test_case_automated_type;
var project_ids;
var test_cases_ids;
var test_plan_id;
var test_suite_ids;
var test_execution_details;


const test_run_automation = async function(){
    project_ids = await testrail.get_project_id(project_name);
    test_cases_ids = await testrail.get_test_cases_list(project_ids, test_case_automated_type);
    test_suite_ids = await testrail.get_test_suites(project_ids);
    //await get_case_types();
    //await get_test_section();
    //test_run_id = await add_test_run(project_id, test_run_name, test_cases_ids);
    //await get_test_run(project_id);
    test_plan_id = await testrail.add_test_plan(project_ids, test_plan_name); //1904
    //test_run_id_list = await get_test_plan(test_plan_id);//1905
    //await get_statuses();
    //test_plan_entry_id = await add_test_plan_entry(test_plan_id, test_run_name, test_cases_ids, test_suite_ids);
    test_execution_details={
        project_ids : project_ids,
        test_cases_ids : test_cases_ids,
        test_suite_ids : test_suite_ids,
        test_plan_id : test_plan_id
    };
    module.exports.project_ids = project_ids;
    module.exports.test_suite_ids = test_suite_ids;
    module.exports.test_cases_ids = test_cases_ids;
    module.exports.test_plan_id = test_plan_id;
    module.exports.test_execution_details = test_execution_details;
    console.log(test_execution_details);
    return test_execution_details;
}

module.exports = { project_ids, test_cases_ids, test_plan_id, test_suite_ids, 
    test_run_automation, test_execution_details
};