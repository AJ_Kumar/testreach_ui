const testrail = require('./testrail.js');
const before_execution = require('./before_execution.js');

var test_result_list = []; 

const browser_test_result_list = function(browser){
    let tests, passed, errors, failed, skipped, lasterror_message='', stacktrace='';
    let test_module = browser.currentTest.module;
    test_result_list = []; 
    browser_tc_object = Object.keys(browser.currentTest.results.testcases);
    browser_tc_object.map( ( browser_tc_object ) => {
      tests = browser.currentTest.results.testcases[browser_tc_object].tests;
      passed = browser.currentTest.results.testcases[browser_tc_object].passed;
      errors = browser.currentTest.results.testcases[browser_tc_object].errors;
      failed = browser.currentTest.results.testcases[browser_tc_object].failed;
      skipped = browser.currentTest.results.testcases[browser_tc_object].skipped;
      if(failed){
        lasterror_message = browser.currentTest.results.testcases[browser_tc_object].lastError['message'];
        stacktrace = browser.currentTest.results.testcases[browser_tc_object].stackTrace;
      }
      /** pattern matching to reterive TC id from the given string
      **input : [4472, C4473] Admin Login test
      **output: 4472 4473
      **/
      browser_tc_id_list = browser_tc_object.replace(/(^.*\[|[~\,*]|[~\C*]|\].*$)/g, '').split(' ');
      browser_tc_id_list.map( ( browser_tc_id_list ) => {
        let test_result = {
          id : browser_tc_id_list,
          module : test_module,
          total : tests,
          passed : passed,
          errors : errors,
          failed : failed,
          skipped : skipped,
          error_message : lasterror_message+'\n'+stacktrace
        };
        test_result_list.push(test_result);
      });
     
    });
   //console.log('browser_tc_list',browser_tc_list);
   return test_result_list
}
  
const update_results = async function(test_plan_id, test_result_list, test_status, test_suite_ids){
    let test_case_id, test_case_failed, test_case_skipped, test_case_passed, 
    test_case_total, test_case_error_message, content_filter, test_case_module,
    test_run_id_list, test_details_list;
    let test_case_id_list = [];
    test_result_list.map( async ( test_result_list ) => {
      test_case_id_list.push(Number(test_result_list.id));
      test_case_module = test_result_list.module;
    });
    test_run_id_list =  await testrail.add_test_plan_entry(test_plan_id,  test_case_module, test_case_id_list, test_suite_ids);
    test_details_list =  await testrail.get_test_cases_run(test_run_id_list);  
    test_result_list.map( async ( test_result_list ) => {
      test_case_id= Number(test_result_list.id);
      test_case_module = test_result_list.module;
      test_case_failed = test_result_list.failed;
      test_case_skipped = test_result_list.skipped;
      test_case_passed = test_result_list.passed;
      test_case_total = test_result_list.total;
      test_case_error_message = test_result_list.error_message;
      if(test_case_total === test_case_passed) {
        content_filter = {
          status_id : test_status.passed
        };
      }
      else if((test_case_failed)) {
        content_filter = {
          status_id : test_status.failed,
          comment : test_case_error_message
        };
      }
      else if(test_case_skipped) {
        content_filter = {
          status_id : test_status.retest,
          comment : test_case_error_message
        };
      }
      test_details_list.map( ( test_details_list ) => {
        if(test_details_list.case_id === test_case_id) {
            testrail.add_test_result(test_details_list.run_id, test_details_list.case_id, content_filter);
          //console.log(results);
        }
      });
    });
}
  
const update_automation_results = async function(browser){
  const test_status = testrail.test_status;
  const test_plan_id = await before_execution.test_plan_id;
  const test_suite_ids = await before_execution.test_suite_ids;


    test_result_list =  await browser_test_result_list(browser);
    
    await update_results(test_plan_id, test_result_list, test_status, test_suite_ids);
    //console.log("updated_test_plan_id");
    //add_test_plan_entry(test_plan_id,  test_run_name, test_case_id, test_suite_ids);

}

module.exports = { browser_test_result_list, update_results, update_automation_results,
    test_result_list
};