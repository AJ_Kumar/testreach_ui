//var testrail = require("./custom_commands/testrail.js");
const TestRail = require("testrail-api");
const testrail_url= 'https://testreach.testrail.net/';
const testrail_login_id= 'akumar@testreach.com';
const testrail_password= 'Friendship@4';

const today_date = new Date().getDate()+'-'+(new Date().getMonth()+1)+'-'+new Date().getFullYear();
const test_plan_name= 'Automation Testing '+today_date;
const project_name= 'TestReach';
//const test_run_name= 'Smoke Test '+today_date;

/*{ id: 2, name: 'Accessibility', is_default: false },
  { id: 3, name: 'Automated', is_default: false },
  { id: 4, name: 'Compatibility', is_default: false },
  { id: 6, name: 'Functional', is_default: false },
  { id: 14, name: 'Navigation', is_default: false },
  { id: 8, name: 'Performance', is_default: false },
  { id: 9, name: 'Regression', is_default: false },
  { id: 10, name: 'Security', is_default: false },
  { id: 12, name: 'Usability', is_default: true },
  { id: 15, name: 'Validation', is_default: false },
  { id: 13, name: 'Verification', is_default: false } */
const test_case_automated_type = 3; 

/*
Test status for result
1 Passed
5 Failed
4 Retest
2 Blocked
6 Invalid
3 Untested
*/
const test_status = {
  passed : 1,
  failed : 5,
  untested : 3,
  retest : 4
}

var project_ids = [];
var test_cases_id_list = [];
var test_run_id = [];
var test_plan_id = [];
var test_suite_ids = [];
var test_run_id_list = [];
var test_id_list = [];
var test_result_list = [];
var test_details_list = [];

 
const testrail = new TestRail({
  host: testrail_url,
  user: testrail_login_id,
  password: testrail_password
});


const get_project_id = function(project_name){
  project_ids = [];
  return testrail
  .getProjects(/*FILTERS=*/{})
  .then((result) =>{
    //projects = JSON.parse(projects);
    projects = result.body;
    projects.map( ( projects ) => {
      if(projects.name === project_name)
        { project_ids.push(projects.id);}
    });
    return project_ids;
  })
  .catch(function (error) {
    console.log(error.message);
  })
}

const get_test_cases_list = function(project_ids, test_case_automated_type){
  let test_cases_filter = {
    type_id:test_case_automated_type
  }
  test_cases_id_list=[];
  return testrail
  .getCases(/*PROJECT_ID=*/project_ids, /*FILTERS=*/test_cases_filter)
  .then(function (result) {
    //console.log('result:', result.body);
    test_cases_list = result.body;
    test_cases_list.map( ( test_cases_list ) => {
      test_cases_id_list.push(test_cases_list.id);
        });
  return test_cases_id_list;  
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}

const get_test_suites = function(project_ids){
  test_suite_ids=[];
  return testrail
  .getSuites(/*PROJECT_ID=*/project_ids)
  .then(function (result) {
    //console.log('result:', result.body);
    test_suite_list = result.body;
    test_suite_list.map( ( test_suite_list ) => {
      test_suite_ids.push(test_suite_list.id);
        });
  return test_suite_ids;  
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}

const get_test_section = function(test_section_id){
  return testrail
  .getSection(/*SECTION_ID=*/test_section_id)
  .then(function (result) {
    console.log('get_test_section', result.body);
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}

const get_case_types = function(){
  return testrail
  .getCaseTypes()
  .then(function (result) {
    console.log('get_case_types', result.body);
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}

const add_test_run = function(project_ids, test_run_name, test_cases_ids){
  let test_run_content = {
    name: test_run_name,
    include_all: false,
    case_ids: test_cases_ids
  }
  test_run_id=[];
  return testrail
  .addRun(/*PROJECT_ID=*/project_ids, /*CONTENT=*/test_run_content)
  .then(function (result) {
    test_run_id.push(result.body.id);
    return test_run_id;  
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}

const get_test_run = function(project_ids){
  /*let test_run_content = {
    name: test_run_name,
    include_all: false,
    case_ids: test_cases_ids
  }*/
  return testrail
  .getRuns(/*PROJECT_ID=*/project_ids, /*FILTERS=*/)
  .then(function (result) {
    console.log('get_test_run', result.body);
    //test_run_id.push(result.body.id);
    //return test_run_id;  
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}

const add_test_plan = function(project_ids, test_plan_name){
  let test_plan_entries = {
    name: test_plan_name,/*
    entries: [{
      suite_id:Number(test_suite_ids),
      name: test_run_name,
      include_all: false,
      case_ids: test_cases_ids,
      runs:[{
        name: test_run_name,
        include_all: false, // Custom selection
        case_ids: test_cases_ids
        }]
    }]*/
  }
  test_plan_id=[];
  return testrail
  .addPlan(/*PROJECT_ID=*/project_ids, /*CONTENT=*/test_plan_entries)
  .then(function (result) {
    //console.log('add_test_plan', result.body);
    test_plan_id.push(result.body.id);
    return test_plan_id;  
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}

const add_test_plan_entry = function(test_plan_id,  test_run_name, test_cases_ids, test_suite_ids){
  let test_plan_entries = {
    suite_id:Number(test_suite_ids),
    name: test_run_name,
    include_all: false,
    case_ids: test_cases_ids,/*
      runs:[{
        name: test_run_name,
        include_all: false, // Custom selection
        case_ids: test_cases_ids
        }]*/
  }
  test_run_id_list=[];
  return testrail
  .addPlanEntry(/*PLAN_ID=*/test_plan_id, /*CONTENT=*/test_plan_entries)
  .then(function (result) {
    test_run_ids = result.body.runs;
    test_run_ids.map( ( test_run_ids ) => {
      test_run_id_list.push(test_run_ids.id);
    }); 
  return test_run_id_list
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}

const get_test_plan = function(test_plan_id){
  test_run_id_list=[];
  return testrail
  .getPlan(/*PLAN_ID=*/test_plan_id)
  .then(function (result) {
    get_test_entries_list = result.body.entries;
    get_test_entries_list.map( ( get_test_entries_list ) => {
      get_test_runs_list = get_test_entries_list.runs;
      get_test_runs_list.map( ( get_test_runs_list ) => {
        test_run_id_list.push(get_test_runs_list.id);
      });
    });
    return test_run_id_list;  
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}


const get_statuses = function(){
  return testrail
  .getStatuses()
  .then(function (result) {
    //console.log('getStatusesList', result.body);
    test_statuses_list = result.body;
    test_statuses_list.map( ( test_statuses_list ) => {
      console.log('getStatusesList', test_statuses_list.id, test_statuses_list.label);
        });
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}

const get_test_cases_run = function(test_run_id_list){
  let content_filter = {status_id : test_status.untested};
  test_details_list=[];
  return testrail
  .getTests(/*RUN_ID=*/Number(test_run_id_list), /*FILTERS=*/{})
  .then(function (result) {
    test_details_lists = result.body;
    test_details_lists.map( ( test_details_lists ) => {
      let test_list = {
        id : test_details_lists.id,
        case_id : test_details_lists.case_id,
        status_id : test_details_lists.status_id,
        run_id :  test_details_lists.run_id
      };
      test_details_list.push(test_list);
        });
    return test_details_list;  
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}

const add_test_result = function(test_run_id, test_case_id, content_filter){
  testrail
  .addResultForCase(/*RUN_ID=*/test_run_id, /*CASE_ID=*/test_case_id, /*CONTENT=*/content_filter)
  .then(function (result) {
    console.log('result',results);
    //return test_details_list;  
  })
  .catch(function (error) {
    console.log('error', error.message);
  })
}


module.exports = {  get_project_id, get_test_cases_list, get_test_suites, get_test_section, 
  get_case_types, add_test_run, get_test_run, add_test_plan, add_test_plan_entry,get_test_plan, 
  get_statuses, get_test_cases_run, add_test_result, test_case_automated_type, test_plan_name, 
  project_name, test_status
};