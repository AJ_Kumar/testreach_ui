// Get Selenium and the drivers
const seleniumServer = require('selenium-server');
const chromedriver = require('chromedriver');
const geckodriver = require('geckodriver');

const SCREENSHOT_PATH = 'screenshots';
//const TESTS_PATH = ['tests'];
const TESTS_PATH = ['tests'];
const PAGES_PATH = ['pages'];
const OUTPUT_PATH = 'reports';
const GLOBALS_PATH = './globals.js';
const CUSTOM_COMMANDS_PATH = ['custom_commands'];
const CUSTOM_ASSERTIONS_PATH = '';
const URL = 'https://dev.testreach.com/';
const ADMIN_LOGIN_ID = 'akumar+automationorg@testreach.com';
const ADMIN_LOGIN_PASSWORD = 'tr123';
const CANDIDATE_LOGIN_ID = 'akumar+can2@testreach.com';
const CANDIDATE_LOGIN_PASSWORD = 'Friendship@4';
const TESTRAIL_URL = 'https://testreach.testrail.net/';
const TESTRAIL_LOGIN_ID = 'akumar@testreach.com';
const TESTRAIL_LOGIN_PASSWORD = 'Friendship@4';

var config = {
  src_folders: TESTS_PATH,
  page_objects_path: PAGES_PATH,
  output_folder: OUTPUT_PATH, // Where to output the test reports
  custom_commands_path: CUSTOM_COMMANDS_PATH,
  custom_assertions_path: CUSTOM_ASSERTIONS_PATH,
  globals_path: GLOBALS_PATH,
  selenium: {
    // Information for selenium, such as the location of the drivers ect.
    start_process: true,
    server_path: seleniumServer.path,
    port: 4444, // Standard selenium port
    cli_args: {
      'webdriver.chrome.driver': chromedriver.path,
      'webdriver.gecko.driver': geckodriver.path
    }
  },
  test_workers: {
    // This allows more then one browser to be opened and tested in at once
    enabled: false,
    workers: 'auto'
  },
  test_settings: {
    default: {
      screenshots: {
        enabled: false, // if you want to keep screenshots
        path: SCREENSHOT_PATH // save screenshots here
      },
      launch_url : URL,
      globals: {
        // How long to wait (in milliseconds) before the test times out
        waitForConditionTimeout: 5000,
        admin_login_id : ADMIN_LOGIN_ID,
        admin_password : ADMIN_LOGIN_PASSWORD,
        candidate_login_id : CANDIDATE_LOGIN_ID,
        candidate_password : CANDIDATE_LOGIN_PASSWORD,
        testrail_url : TESTRAIL_URL,
        testrail_login_id : TESTRAIL_LOGIN_ID,
        testrail_password : TESTRAIL_LOGIN_PASSWORD
      },
      desiredCapabilities: {
        // The default test
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true
      }
    },
    // Here, we give each of the browsers we want to test in, and their driver configuration
    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true
      }
    },
    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true
      }
    },
    safari: {
      desiredCapabilities: {
        browserName: 'safari',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true
      }
    },
    edge: {
      desiredCapabilities: {
        browserName: 'MicrosoftEdge',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true
      }
    }
  }
};


module.exports = config;

function padLeft (count) { // theregister.co.uk/2016/03/23/npm_left_pad_chaos/
  return count < 10 ? '0' + count : count.toString();
}

let FILECOUNT = 0; // "global" screenshot file count
/**
 * The default is to save screenshots to the root of your project even though
 * there is a screenshots path in the config object above! ... so we need a
 * function that returns the correct path for storing our screenshots.
 * While we're at it, we are adding some meta-data to the filename, specifically
 * the Platform/Browser where the test was run and the test (file) name.
 */
function imgpath (browser) {
  let a = browser.options.desiredCapabilities;
  let meta = [a.platform];
  meta.push(a.browserName ? a.browserName : 'any');
  meta.push(a.version ? a.version : 'any');
  meta.push(a.name); // this is the test filename so always exists.
  let metadata = meta.join('~').toLowerCase().replace(/ /g, '');
  return SCREENSHOT_PATH + metadata + '_' + padLeft(FILECOUNT++) + '_';
}

module.exports.imgpath = imgpath;
module.exports.SCREENSHOT_PATH = SCREENSHOT_PATH;
/*
const testrail = {
  url: 'https://testreach.testrail.net/',
  login_id: 'akumar@testreach.com',
  password: 'Friendship@4',
};

const admin = {
  url: 'https://dev.testreach.com/',
  login_id: 'akumar@testreach.com',
  password: 'Friendship@4',
};
const candidate = {
  url: 'https://dev.testreach.com/',
  login_id: 'akumar+can2@testreach.com',
  password: 'Friendship@4',
};

module.exports.testrail = testrail;
module.exports.admin = admin;
module.exports.candidate = candidate;
*/
